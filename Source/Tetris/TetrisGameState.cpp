// Fill out your copyright notice in the Description page of Project Settings.

#include "Tetris.h"
#include "TetrisGameState.h"
#include "FigureBaseActor.h"
#include "BlockActor.h"
#include "PaperSpriteComponent.h"
#include "TFigure.h"
#include "IFigure.h"
#include "SFigure.h"
#include "ZFigure.h"
#include "OFigure.h"
#include "LFigure.h"

ATetrisGameState::ATetrisGameState() {
	fields.AddDefaulted(200);
	fields.Shrink();
	for (int y = 0; y != 20; ++y) {
		for (int x = 0; x != 10; ++x) {
			fields[y * 10 + x].x = x + 1;
			fields[y * 10 + x].y = y + 1;
		}
	}
}

void ATetrisGameState::OnFigurePlased(class AFigureBaseActor * actor) {
	TArray<FVector> tmp;
	for (auto& a : actor->sprites) {
		tmp.Push(a->GetComponentLocation());
	}
	actor->Destroy();
	MakeBlocksFromFigure(tmp);
	bool Destro = false;
	for (int i = 1; i < 21; ++i) {
		bool tmp1 = false;
		tmp1 = IsRowFilled(i);
		Destro |= tmp1;
		if (tmp1) {
			DestroyRow(i);
		}
	}
	//if(Destro)StackAfterDestroy();
	SpawnNextFigure();
}

void ATetrisGameState::BeginPlay() {
	Super::BeginPlay();
	SpawnNextFigure();
}

void ATetrisGameState::AddScore(int num) {
	score += num;
	OnScoreChanged.Broadcast(score);
}

void ATetrisGameState::SpawnNextFigure() {
	FVector v(500, 2150, 0);
	FRotator r(0, 0, 0);
	srand(FDateTime::Now().GetMillisecond());
	switch (rand() % 6) {
	case 0:GetWorld()->SpawnActor(ATFigure::StaticClass(), &v, &r); break;
	case 1:GetWorld()->SpawnActor(AIFigure::StaticClass(), &v, &r); break;
	case 2:GetWorld()->SpawnActor(ASFigure::StaticClass(), &v, &r); break;
	case 3:GetWorld()->SpawnActor(AZFigure::StaticClass(), &v, &r); break;
	case 4:GetWorld()->SpawnActor(AOFigure::StaticClass(), &v, &r); break;
	case 5:GetWorld()->SpawnActor(ALFigure::StaticClass(), &v, &r); break;
	}
}

void ATetrisGameState::StackAfterDestroy() {
	for (int i = fields.Num() - 1; i > -1; --i) {
		if (fields[i].bFilled && fields[i].y<20) {
			int move_to = 0;
			for (int k = 1; k != 20; ++k) {
				bool a_err = false;
				Field& a = at(fields[i].x, fields[i].y + k, a_err);
				if (fields[i].y + move_to == 20)break;
				if (a.bFilled && !a_err) {
					break;
					move_to = k;
				}
			}
			MoveBlock(fields[i].x, fields[i].y, fields[i].x, fields[i].y + move_to);
		}
	}
}

void ATetrisGameState::MoveBlock(int from_col, int form_row, int to_col, int to_row) {
	bool err1 = false, err2 = false;
	Field& from = at(from_col, form_row, err1);
	Field& to = at(to_col, to_row, err2);
	if (err1 || err2)return;
	to.block = from.block;
	to.bFilled = true;
	FVector loc = from.block->GetActorLocation();
	from.block->SetActorLocation(FVector(loc.X, loc.Y - 100.f, 0));
	from.block = 0;
	from.bFilled = false;
}

void ATetrisGameState::MakeBlocksFromFigure(TArray<FVector> coponents_pos) {
	for (auto& p : coponents_pos) {
		Field& tmp = at(ComponentPosToFieldCoords(p.GridSnap(100.f)));
		tmp.bFilled = true;
		FVector pos = p.GridSnap(100.f);
		FRotator t(0, 0, 0);
		FActorSpawnParameters s;
		s.bNoCollisionFail = true;
		tmp.block = Cast<ABlockActor>(GetWorld()->SpawnActor(ABlockActor::StaticClass(), &pos, &t, s));
	}
}

void ATetrisGameState::DestroyRow(int row) {
	for (int i = 1; i < 11; ++i) {
		Field& tmp = at(i, row);
		tmp.bFilled = false;
		tmp.block->Destroy();
		tmp.block = 0;
	}
	AddScore(100);
}

ATetrisGameState::Field& ATetrisGameState::at(int col, int row, bool& error) {
	if (col > 10 || row > 20 || row < 1 || col < 1) { error = true; return fields[0]; } else { error = false; return  fields[(row - 1) * 10 + (col - 1)]; }
}

ATetrisGameState::Field& ATetrisGameState::at(int col, int row) {
	return fields[(row - 1) * 10 + (col - 1)];
}

ATetrisGameState::Field& ATetrisGameState::at(std::pair<int, int> col_row) {
	return at(col_row.first, col_row.second);
}

bool ATetrisGameState::IsRowFilled(int row) {
	bool rv = true;
	for (int i = 1; i < 11; ++i) {
		Field& tmp = at(i, row);
		rv &= tmp.bFilled;
		//GEngine->AddOnScreenDebugMessage(i + 50, 1, FColor::White, FString::FromInt(tmp.bFilled ? 1 : 0));
	}
	return rv;
}

std::pair<int, int> ATetrisGameState::ComponentPosToFieldCoords(FVector pos) {
	//GEngine->AddOnScreenDebugMessage(0, 5, FColor::White, (FVector(11, 20, 0) - (pos) / 100).ToString());
	return std::pair<int, int>(11 - (pos / 100).X, 20 - (pos / 100).Y);
}


