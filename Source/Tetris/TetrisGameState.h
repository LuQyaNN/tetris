// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/GameState.h"
#include "FigureBaseActor.h"
#include <utility>
#include "TetrisGameState.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnScoreChanged, int, CurrentScore);
/**
*
*/
UCLASS()
class TETRIS_API ATetrisGameState : public AGameState {
	GENERATED_BODY()
public:
	ATetrisGameState();
	TWeakObjectPtr<AFigureBaseActor> CurrentFigure;
	void OnFigurePlased(class AFigureBaseActor* actor);
	virtual void BeginPlay() override;
	struct Field {
		class ABlockActor* block = 0;
		bool bFilled = false;
		int x = 0;
		int y = 0;
	};
	TArray<Field> fields;

	UPROPERTY(BlueprintAssignable)
		FOnScoreChanged OnScoreChanged;
private:
	void AddScore(int num);
	void StackAfterDestroy();
	void MoveBlock(int from_col, int form_row, int to_col, int to_row);
	void SpawnNextFigure();
	void MakeBlocksFromFigure(TArray<FVector> coponents_pos);
	void DestroyRow(int row);
	Field& at(int col, int row, bool& error);
	Field& at(int col, int row);
	Field& at(std::pair<int, int> col_row);
	bool IsRowFilled(int row);
	std::pair<int, int> ComponentPosToFieldCoords(FVector pos);
	int score = 0;
};
