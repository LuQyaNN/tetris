// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "FigureBaseActor.h"
#include "OFigure.generated.h"

/**
 * 
 */
UCLASS()
class TETRIS_API AOFigure : public AFigureBaseActor
{
	GENERATED_BODY()
public:
	AOFigure();
	void Rotate()override {};
};
