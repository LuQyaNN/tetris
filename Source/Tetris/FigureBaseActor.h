// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "PaperSpriteComponent.h"
#include "FigureBaseActor.generated.h"

UCLASS()
class TETRIS_API AFigureBaseActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFigureBaseActor();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	virtual void Move();

	virtual void Rotate();
	
	virtual void Swap(bool bToLeft,bool bToRight);

	void Hastle(bool a);
	
	TArray<UPaperSpriteComponent*> sprites;
private:
	class ATetrisGameState* gs;
	float speed=1;
};
