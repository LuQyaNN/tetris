// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "FigureBaseActor.h"
#include "IFigure.generated.h"

/**
 * 
 */
UCLASS()
class TETRIS_API AIFigure : public AFigureBaseActor
{
	GENERATED_BODY()
public:
		AIFigure();
};
