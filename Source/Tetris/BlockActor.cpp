// Fill out your copyright notice in the Description page of Project Settings.

#include "Tetris.h"
#include "BlockActor.h"
#include "PaperSprite.h"

ABlockActor::ABlockActor(){
	static ConstructorHelpers::FObjectFinder<UPaperSprite> spr(TEXT("PaperSprite'/Game/Block_Sprite.Block_Sprite'"));
	GetRenderComponent()->SetSprite(spr.Object);
	GetRenderComponent()->Mobility =EComponentMobility::Movable;
	SetActorRotation(FQuat(1, 0, 0, 1));
	GetRenderComponent()->SetWorldScale3D(FVector(0.98, 0.98, 0.98));
	GetRenderComponent()->SetCollisionProfileName(TEXT("Block"));
	GetRenderComponent()->SetNotifyRigidBodyCollision(true);
}


