// Fill out your copyright notice in the Description page of Project Settings.

#include "Tetris.h"
#include "IFigure.h"

AIFigure::AIFigure() {
	sprites[1]->AddWorldOffset(FVector(-100, 0, 0));
	sprites[2]->AddWorldOffset(FVector(100, 0, 0));
	sprites[3]->AddWorldOffset(FVector(200, 0, 0));
}
