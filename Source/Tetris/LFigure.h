// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "FigureBaseActor.h"
#include "LFigure.generated.h"

/**
 * 
 */
UCLASS()
class TETRIS_API ALFigure : public AFigureBaseActor
{
	GENERATED_BODY()
public:
	ALFigure();
};
