// Fill out your copyright notice in the Description page of Project Settings.

#include "Tetris.h"
#include "TetrisGameMode.h"
#include "TetrisPawn.h"
#include "TetrisGameState.h"

ATetrisGameMode::ATetrisGameMode() {
	DefaultPawnClass = ATetrisPawn::StaticClass();
	GameStateClass = ATetrisGameState::StaticClass();
}



