// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "PaperSpriteActor.h"
#include "BlockActor.generated.h"

/**
 * 
 */
UCLASS()
class TETRIS_API ABlockActor : public APaperSpriteActor
{
	GENERATED_BODY()
public:
	ABlockActor();
};
