// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "FigureBaseActor.h"
#include "SFigure.generated.h"

/**
 * 
 */
UCLASS()
class TETRIS_API ASFigure : public AFigureBaseActor
{
	GENERATED_BODY()
public:
	ASFigure();
};
