// Fill out your copyright notice in the Description page of Project Settings.

#include "Tetris.h"
#include "FigureBaseActor.h"
#include "PaperSprite.h"
#include "TetrisGameState.h"



// Sets default values
AFigureBaseActor::AFigureBaseActor() {
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	sprites.AddUninitialized(4);
	static ConstructorHelpers::FObjectFinder<UPaperSprite> spr(TEXT("PaperSprite'/Game/Block_Sprite.Block_Sprite'"));
	for (int i = 0; i < 4; ++i) {
		if (i == 0) {
			sprites[i] = CreateDefaultSubobject<UPaperSpriteComponent>(TEXT("Origin"));
			RootComponent = sprites[i];
		} else {
			FString name("sprite");
			name += FString::FromInt(i);
			sprites[i] = CreateDefaultSubobject<UPaperSpriteComponent>(FName(*name));
			sprites[i]->AttachTo(sprites[0]);
		}
		sprites[i]->SetWorldRotation(FQuat(1, 0, 0, -1));
		sprites[i]->SetSprite(spr.Object);
		sprites[i]->SetNotifyRigidBodyCollision(true);
		sprites[i]->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
		sprites[i]->SetCollisionProfileName(TEXT("Figure"));
		sprites[i]->SetWorldScale3D(FVector(0.98, 0.98, 0.98));
	}
	SetActorEnableCollision(true);
}

// Called when the game starts or when spawned
void AFigureBaseActor::BeginPlay() {
	Super::BeginPlay();
	gs = Cast<ATetrisGameState>(UGameplayStatics::GetGameState(GetWorld()));
	gs->CurrentFigure = this;
}

// Called every frame
void AFigureBaseActor::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);
	Move();

}

void AFigureBaseActor::Hastle(bool a) {
	a ? speed *= 5 : speed = 1;
}

void AFigureBaseActor::Move() {
	for (auto& sprite : sprites) {
		FVector location = sprite->GetComponentLocation();
		if (location.Y <= 50.f) { gs->OnFigurePlased(this); return; }
		TArray<UPrimitiveComponent*> a;
		sprite->GetOverlappingComponents(a);
		if (a.Num() != 0)gs->OnFigurePlased(this);
	}
	AddActorWorldOffset(FVector(0, -5 * GetWorld()->GetDeltaSeconds()*speed * 50, 0));
}

void AFigureBaseActor::Rotate() {
	AddActorWorldRotation(FQuat(0, 0, 1, 1));
	for (auto& sprite : sprites) {
		FVector location = sprite->GetComponentLocation();
		TArray<UPrimitiveComponent*> a;

		sprite->GetOverlappingComponents(a);
		if (location.X <= 0.f || location.X >= 1000.f || location.Y <= 0.f || a.Num() != 0) { AddActorWorldRotation(FQuat(0, 0, 1, -1)); return; }
	}
}

void AFigureBaseActor::Swap(bool bToLeft, bool bToRight) {
	if (bToRight) {
		AddActorWorldOffset(FVector(-100, 0, 0));

		for (auto& sprite : sprites) {
			FVector location = sprite->GetComponentLocation();
			TArray<UPrimitiveComponent*> a;

			sprite->GetOverlappingComponents(a);
			if (location.X <= 0.f || a.Num() != 0) { AddActorWorldOffset(FVector(100, 0, 0)); return; }
		}
	}
	if (bToLeft) {
		AddActorWorldOffset(FVector(100, 0, 0));

		for (auto& sprite : sprites) {
			FVector location = sprite->GetComponentLocation();
			TArray<UPrimitiveComponent*> a;

			sprite->GetOverlappingComponents(a);
			if (location.X >= 1050.f || a.Num() != 0) { AddActorWorldOffset(FVector(-100, 0, 0)); return; }
		}
	}
}

