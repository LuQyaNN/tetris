// Fill out your copyright notice in the Description page of Project Settings.

#include "Tetris.h"
#include "TetrisPawn.h"
#include "TetrisGameState.h"


// Sets default values
ATetrisPawn::ATetrisPawn() {
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	Root = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	RootComponent = Root;
	Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	Camera->SetAbsolute(true, true, true);
	Camera->AttachTo(GetRootComponent());
	Camera->SetWorldLocationAndRotation(FVector(550, 1050, 1000), FRotator(-90, 90, 0));
	Camera->SetProjectionMode(ECameraProjectionMode::Orthographic);
	Camera->SetOrthoWidth(1200);
	Camera->SetConstraintAspectRatio(true);
	Camera->SetAspectRatio(0.5);
}

// Called when the game starts or when spawned
void ATetrisPawn::BeginPlay() {
	Super::BeginPlay();
	gs = Cast<ATetrisGameState>(UGameplayStatics::GetGameState(GetWorld()));
}

// Called every frame
void ATetrisPawn::Tick(float DeltaTime) { Super::Tick(DeltaTime); }

// Called to bind functionality to input
void ATetrisPawn::SetupPlayerInputComponent(class UInputComponent* InputComponent) {
	Super::SetupPlayerInputComponent(InputComponent);
	InputComponent->BindAction(TEXT("Right"), EInputEvent::IE_Pressed, this, &ATetrisPawn::Right);
	InputComponent->BindAction(TEXT("Left"), EInputEvent::IE_Pressed, this, &ATetrisPawn::Left);
	InputComponent->BindAction(TEXT("Rotate"), EInputEvent::IE_Pressed, this, &ATetrisPawn::Rotate);
	InputComponent->BindAction(TEXT("SpeedUp"), EInputEvent::IE_Pressed, this, &ATetrisPawn::SpeedUp);
	InputComponent->BindAction(TEXT("SpeedUp"), EInputEvent::IE_Released, this, &ATetrisPawn::SpeedDown);
}

void ATetrisPawn::Right() { gs->CurrentFigure->Swap(false, true); }

void ATetrisPawn::Left() { gs->CurrentFigure->Swap(true, false); }

void ATetrisPawn::Rotate() { gs->CurrentFigure->Rotate(); }

void ATetrisPawn::SpeedUp() { gs->CurrentFigure->Hastle(true); }

void ATetrisPawn::SpeedDown() { gs->CurrentFigure->Hastle(false); }

